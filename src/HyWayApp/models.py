from django.db import models
from datetime import datetime
from phonenumber_field.modelfields import PhoneNumberField

#****************** Models **********************
class Seller(models.Model):
	"""A class for users"""
	firstname = models.CharField(blank=False, null=False,max_length=50)
	lastname = models.CharField(default="",max_length=50,blank=True)
	email = models.EmailField()
	contact_number = PhoneNumberField()
	address = models.TextField(blank=True,null=True) 
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	password = models.CharField(max_length=50)

	def __unicode__(self):
		return self.firstname +" "+self.lastname

class Contact(models.Model):
	fullname = models.CharField(max_length=100,blank=True,null=True) #blank=False, null=False, 
	email = models.EmailField(blank=True,null=True)
	message = models.TextField(blank=True,null=True) #blank=False,null=False
	created = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self):
		return self.email 


