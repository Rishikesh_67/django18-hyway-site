# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0004_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='created_at',
            new_name='created',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='updated_at',
            new_name='updated',
        ),
    ]
