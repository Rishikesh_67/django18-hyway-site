# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0012_remove_seller_password'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='password',
            field=models.CharField(default=datetime.datetime(2017, 3, 24, 19, 37, 13, 154131, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
    ]
