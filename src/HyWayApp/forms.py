from django import forms
from .models import Contact, Seller
from captcha.fields import CaptchaField

class ContactForm(forms.ModelForm):
	captcha = CaptchaField()
	class Meta:
		model = Contact
		exclude = ["created"]

		widgets = {
			"fullname":forms.TextInput(attrs={"placeholder":"Enter your full name"}),
			"email" : forms.TextInput(attrs={"placeholder":"Enter your valid contact email"}),
			"message":forms.Textarea(attrs={"placeholder":"Contact Message"}),
		}

	def clean_email(self):
		email = self.cleaned_data["email"]
		print "Entered email : ",email
		if email == "":
			raise forms.ValidationError("email is required for contact")
		else:
			return email

	def clean_fullname(self):
		fullname = self.cleaned_data["fullname"]
		print "Entered fullname : ",fullname
		if fullname == "":
			raise forms.ValidationError("fullname is required for contact")
		else:
			return fullname


	def clean_message(self):
		message = self.cleaned_data["message"]
		print "Entered message : ",message
		if message == "":
			raise forms.ValidationError("message is required for contact")
		else:
			return message


class SellerForm(forms.ModelForm):
	confirm_password = forms.CharField(max_length=20,widget=forms.TextInput(attrs={"placeholder":"***********"}))
	captcha = CaptchaField()
	class Meta:
		model = Seller
		exclude = ["created","updated"]
		widgets = {
			'contact_number':forms.TextInput(attrs={"placeholder":"eg. +917353787704 (with dialing code)"}),
			"firstname":forms.TextInput(attrs={"placeholder":"Issac"}),
			"lastname":forms.TextInput(attrs={"placeholder":"Thompson"}),
			"email":forms.TextInput(attrs={"placeholder":"issac.thompson@gmail.com"}),
			"address":forms.Textarea(attrs={"placeholder":"Near pearson building,\nKoramangala, Bangalore, 560047\nKarnataka, India"}),
			"password":forms.TextInput(attrs={"placeholder":"***********"}),
			# "confirm_password" :),
		}

